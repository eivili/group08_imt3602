## Group members
Viktor Kind Svendsen
Eivind Lindstad

### Project Description The main goal of this project was to develop a prototype application for Buypass and Mobai called ESIV. ESIV stands for Efficient and Secure Identity Verification and is supposed to securely deliver biometric data from the passport and an authentic picture of the user for identity verification. ESIV is meant to be a starting point for Buypass and Mobai when implementing their service on Android Mobile devices.

### Strengths and weaknesses of Java

Java is considered by many developers to be a “simple” language, this is due to a pretty standard object-oriented syntax, which lets you form standard and reusable code. It also uses automatic memory allocation and garbage collection, depending on the application this may be a plus point or a minus point, in our case it was positive. This is because Buypass and Mobai specifically told us to focus on the functionality rather than the security, as this application was a prototype for the upcoming main-application.  Integrated memory allocation and garbage collection in any language save the developer for a lot of time spent visualizing their memory allocation. 

Java uses references, which is considered by many to be a more secure alternative to pointers. In an application like ours, security has a high priority, so choosing the right language is definitely something you want to do early on. 
Apart from this, it has a security manager that defines access to classes.

The most noticeable weakness that affected our work was Java’s memory-consumption. Since Java is not a native language for android, some features can feel slower than natively compiled languages such as C++ or Objective C for iOS. This can be felt on memory heavy features like the OCR we use in our application found on: ```https://gitlab.com/eivili/buypassidregistration/-/blob/master/app/src/main/java/com/example/buypassidreg/OcrManager.java```.


### Process and communication systems
As we knew communication was going to be a challenge, we started to prepare and discuss this already in December 2019. One of the group members were planning on international travel to Japan and had intentions of staying there up to mid-March. We also discussed our sleeping schedules, and it became obvious that in addition to different time zones, we also had very different preferred working hours. So when we looked into alternatives for communication we agreed to use:
```Discord``` as a main communication service for communication within the group
```Google hangouts``` for meetings with the Project Owner (PO)
```Slack``` For unofficial questions about the project 
```Email``` for planning of meetings
```Skype``` for meetings with our supervisor due to Covid-19
```Trello``` for kanban and task distribution
```Overleaf``` For shared latex document
```Android Studio``` For application development




#### Experience of Working With Android Studio
Working with Android Studio has been a lot easier and less time-consuming than developing
applications from scratch. The team also had prior experience using this IDE, so there was no competing alternative in sight. Using Android Studios made building the project easy, and it was also helpful in regards to convenient shortcuts like autocomplete functionality etc.

#### Experience of Working With Trello
The group seemed quite naive and optimistic when it came to the usage of Trello. After about a week, our main developer Eivind was the only person who stuck to it, discord replaced it, as well as most of the communication either happened on discord or in the group meetings, which we had several of each week. 

#### Experience of Working With Slack
Slack was the communication tool we used to talk to Mobai (part of the project owners). Using Slack felt like less of a formal setting than e-mail, but more formal than something like Discord. We used Slack to ask questions regarding the development and also asked for advice on specific parts of the code.


#### Experience of Working With Overleaf
For writing the thesis, we almost had a default answer of where to write it. We did not know many alternatives to Overleaf, and since everybody had multiple years of experience with working with latex and Overleaf, this was kind of a no-brainer for us. It was a great tool to use, as our supervisor also had access. It allowed us to customize our thesis to such an extent that the end result was as close to what we had in mind as you could possibly get. Also, the possibility to edit together online in real-time was ideal for us, especially since we never met up face to face. Another great part about Overleaf is that we could use the history feature to see who wrote what and when.


### Use of version control
We used Git as our version control system. After making progress on the development, we would push those changes onto our Gitlab project. If for whatever reason there was a need for looking at earlier changes, Gitlab provides a history log and even lets one revert earlier commits. This proved to be a useful feature for us as some commits caused some issues that needed to be investigated. An example of the use of version control was when we managed to mess up after a commit. What most likely happened was that some files were not added properly, or some files were removed from the repository, resulting in the project unable to build properly when trying to run in on Android Studios. To avoid any unnecessary  trouble with trying to fix the project, we simply reverted the repository to the previous version, and manually added the changes done.
Furthermore, we split the Gitlab project into different branches. We had one main branch and one branch each for the two group members. The idea was to avoid trouble by never committing directly to the main branch, but instead, always make sure to commit to our own branch first. If there seemed to be no issues with the new commit, we would then merge the branches to get the main branch up to date. This plan was for the most part followed, with a few exceptions of times we didn’t follow this structure. 

We used issue tracking to structure the development process. From when we started with issue tracking, commits would always be connected to a specific issue. Such an issue would describe the functionality that the commit is supposed to fulfill. The issues were labeled by number, and would usually be assigned to a specific team member. We also tried to create due dates wherever feasible, but these were not always followed as expected due to changes in priorities throughout the development process. Overall, the issue tracking system worked out as an organized way of structuring the work, and also acted as a reminder where due dates were included.



### Programming style 
Here, we will present a list explaining rules for the programming style of the code in our project.

General
The strings used in the GUI will not be hard coded, but rather referenced from a string file containing all the strings
The type of colors that are used frequently will be stored as values in a separate file. 
The dimension sizes frequently used in the GUI will be stored in a separate file.
The first “{“ bracket in a context (if-statements, functions, loops etc) will start on the same line as the statement. This means that we do not make a like break before starting the “{ }”.


Comments
Any process that is not so obvious to understand by looking at it should have a brief comment to explain its purpose.
Comments should only be a few words, and not go into too much depth.

Naming conventions
Most variables will follow the the style of camelCase, e.g. anExample.  This also applies to IDs in layout files.
An exception to the use of camelCase is found in static variables. These variables are all upper case and each word is separated by an underscore, e.g. AN_EXAMPLE
Another exception to this rule is the IDs in layout files where the object is of type “view” and “layout”
All layout files in the project follow the naming style of all lower case letters and each word being separated by an underscore, e.g. an_example. String names in the string files also follow this convention.
All java files in the project will follow the CamelCase naming convention, where the first letter is upper case.
Variables in the java files that refer to object in the GUI will include the object type last in the name of the variable, e.g. mainLayout. Note that some object types are shortened, like button becomes btn.

### Pattern usage

As for our project, we did not prioritize pattern usage as much as we would have liked to. 
Creational design patterns are concerned with the way of creating objects. These design patterns are used when a decision must be made at the time of instantiation of a class.

Some of the patterns we used:

Command pattern
In late March, the Project Owner requested a way of storing some of the client information. We discussed this by customizing a logger, which would send information as a JSON to the database. Soon after the suggestion came up, we made this logger, unfortunately, due to a computer crash, this never got pushed properly. We used a library, Google’s GSON, which does exactly what the command pattern is set up to handle, convert JSON data into a Java-object, as well as being equally able to convert a java object into JSON. This allowed us to easily log all the necessary information the project owner requested.



Model-view-controller pattern
Our approach to activity handling fits the model-view-controller pattern. The different buttons and interactive objects in the user interface acts as a controller. Here, the user can use the model to change the view. But this does not happen directly, because there is a step in between. We intentionally made one part of the code control all the logic for the activities in the application. This part acts as a model, to the system, since it updates the view depending on the actions done by the user in the controller component of the application. At last we have the view component, which is the activity layouts displayed to the user.


### Use of libraries 

Libraries have been an important part of our project. There is no point in making the development process more difficult than it has to be. All of the major parts of the project uses libraries, and these are: 

Text recognition system - Tesseract-OCR
Custom camera - CameraX
Passport RFID chip reader - JMRTD (Java Implementation of Machine Readable Travel Documents)
Helping libraries to decrypt RFID chip information - Spongycastle, Scuba-SC
Facial recognition and facial feature detection system - Google Vision
HTTP request builder - Apache HTTP Components
Date selection input field - MaterialDateTimePicker

These libraries are implemented into the application by adding them as dependencies in the Gradle file of the project. Then, to use their functionality in a Java file, we simply had to import them in that specific file. 

### Professionalism in approach to development
As we were learning about different concepts and ideas of professionalism in programming, we tried integrating the most relevant realistically achievable parts into the development of the application. An example of this 

There were also some parts in professional programming that did not seem to be worth doing in our specific case. This is for example the use of design patterns in our code. It is not that they are completely irrelevant, but trying to force the use of a design pattern felt forced and unnatural. Maybe if the group had more knowledge about patterns in beforehand, we would be more comfortable using them more often in our code.

### Use of code review

The main method of code review done in this project was done when there was some errors or unintended action that needed debugging. During these times we would be in a call together and one person would share their screen, or we would post the error code in the chat and discuss it while Googling around for a solution. 

Also, there was some code review done towards the end of the project. Here one would read the code and note important points of what needed to be changed. This could be things like overflow exploits, bad naming conventions, unused code parts, or untidy code.





### good code
	```https://gitlab.com/eivili/buypassidregistration/-/commit/42f63359c422935e636c4f0770a51a1302fec897```
	The objective behind this commit becomes very clear when starting to read the code. With good comments, good names and code divided up in clear separate parts, the whole code just seems cleaner. Every line/function added has a clear and good message explaining what it does. Prints the stacktrace(), which allows us to read what resorted in a try-catch-exception. 
### bad code

	```https://gitlab.com/eivili/buypassidregistration/-/commit/d9c8eadbeb2c3895074e4edf49fd33ad0e7a6edc```
	This example of code is not what i consider "good" code.
	I would start of by saying the most obvious part first, its not very well commented. When writing this piece of code, I was very stressed out. As Eivind was the most experienced
	programmer on our group, he did most of the coding. I suddenly joined, and got dealt a decent sizeable task, to make a custom camera instead of using the default camera and use the photo in different parts of the application and storing the picture in a secure manner. I googled around, and tried to make some different versions but very few worked optimally without lagging. After getting frustrated, I tried to look for the easiest libraries out there. CameraX came up, with a decent example which i followed.

	I would say what frustrates me about this code, is that its incomplete. The variable and function names are okay, a little generic, but could definitely be better. This code does not have a clear structure, and the use case is very static and simple. Its not dynamic and seems clunky to access and use when in other activities. Dynamic code for me is very important, and static code should be avoided in most cases. Especially in object oriented languages, beeing able to easily access classes and objects "freely" makes it much more of a pleasure to work on for all parts.

	As Eivind wrote most of the code in the project, and I worked on the thesis, there is not too much of my own code to see a clear pattern. I have some experience from Tom Røise's "IMT3110 - Software Design", we touched upon different patterns. When looking at the code & project in retrospect I would have liked to work after a "State Pattern", which restricts what a user can do depending on which state he/she is in. This makes certain operations illegal, and the user is forced to change their state to access different functionaly of the system. Having some classes and functions legal in only certain user-states can provide the developer with a clear overview of what happens and in what order.

	To point it out, the "bad" points of this code is:
	* No comments
	* No testing
	* No clear pattern usage
	* Bad naming


### Before and after refactoring
I tried generating a hidden pastebin link with the code before and after, but it looked kind of messy. I hope its okay to just link the refactoring commit where i changed some things, the main point should be the same.
1. ```https://gitlab.com/eivili/buypassidregistration/-/commit/d5d1ac5f0d749197f08bb4be2d340de1e52cea00```
2. ```https://gitlab.com/eivili/buypassidregistration/-/commit/cbf114553aa530fcbd867f007058abcb20523b76```


So my first approach for the first refactoring was just to read over the code, read over the names, and look for vulnerabilties.
What i noted was that the class name was very bad, functions were missing comments, and i saw a possible vulnerability to an overflow attack. I didnt want to change too much at once, so i read it over again. This is mostly why the commits were split in to two pieces. I had an exam today so i had set a time limit of how much i could work before going to bed.
I changed the name from CustomActivity to CameraActivity.
I cleaned up the syntax so its more easily readable.
I added some comments explaining what functions like createAlert() were doing. 
I changed the hardcoded strings to variables found in the "Strings.xml".
I set up a check which checks the string length of a string in our array " String[] emptyCode = new String[3]; ", which may be vulnerable to an overflow attack.


### Reflection about professionalism in programming

My definition of professional written code is that I can say its easily readable, maintainable, understandable and testable.

When i started programming, i heard alot about good/bad code, how to write good code. My main focus, was to ensure mainly two main points
A - Does the code run?
B - Does it deliver the features that it is supposed to deliver?

Usually it did, and i considered it to be "good code", because i had solved the problem in some fancy new way, but as i learned more, i realised this had
nothing to do, with writing in a professional manner. 
I learned this analogy a while ago, and this is how i kind of define professional programming from someone who does it as a hobby, or is still kind of new in the field. 

One can compare a treehouse to a home, one is written is such a way that its easy to maintain and easily update it. A treehouse on the other hand is slowly going to deteriorate and wear down, because it wasn't really planned all that much and built in a proper way. 

Both are shelters, but one is going to outlast the other. Looking back at this, I think this is one of the main ways to spot a new developer. Sustainability and
maintainability just doesnt get prioritized enough.

Code readability is, as mentioned, very important. Dont write code for yourself or for you client, but write it for the next guy who is going to maintain it. Write the cleanest code possible, the most maintainable code. One should be able to look at the name of the code, read it fast and understand what it does. Writing in a professional manner also includes staying up to date with

Part of writing clean maintanable code is to care about your code. Not just staying up to date on standards, this includes
* Function naming
* Organization of code
* Drive principles
* Solid principles